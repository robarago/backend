package com.craftingjava.examples.twitter;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.messaging.MessageChannel;
import org.springframework.beans.factory.annotation.Value;
import twitter4j.Status;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlite3.SQLitePlugin;

@Slf4j
@Configuration
public class TwitterConfig {

	@Value("${minfollowers:1500}")
	private Integer minfollowers;

	@Value("${languages:es,fr,it}")
	private String[] languages;

	@Value("${tracks:java}")
	private String[] tracks;

	Jdbi jdbi = Jdbi.create("jdbc:sqlite:../assignment.sqlite3")
		.installPlugin(new SQLitePlugin());

  @Bean
  TwitterStreamFactory twitterStreamFactory() {
    return new TwitterStreamFactory();
  }

  @Bean
  TwitterStream twitterStream(TwitterStreamFactory twitterStreamFactory) {
    return twitterStreamFactory.getInstance();
  }

  @Bean
  MessageChannel outputChannel() {
    return MessageChannels.direct().get();
  }

  @Bean
  TwitterMessageProducer twitterMessageProducer(
      TwitterStream twitterStream, MessageChannel outputChannel) {

    TwitterMessageProducer twitterMessageProducer =
        new TwitterMessageProducer(twitterStream, outputChannel);

    twitterMessageProducer.setLanguages(Arrays.asList(this.languages));
    twitterMessageProducer.setTracks(Arrays.asList(this.tracks));

    return twitterMessageProducer;
  }

  @Bean
  IntegrationFlow twitterFlow(MessageChannel outputChannel) {
    return IntegrationFlows.from(outputChannel)
        .handle(m -> {
					Status s = (Status)m.getPayload();
					List<String> hashtags = Arrays.asList(s.getHashtagEntities()).stream()
						.map(h -> h.getText())
						.collect(Collectors.toList());
					if(s.getUser().getFollowersCount() > this.minfollowers) {
						log.info(
							"ID=" + s.getId()
							+ "\nUSER=" + s.getUser().getScreenName()
							+ "\nTEXT=" + s.getText()
							+ "\nLOCATION=" + s.getUser().getLocation()
							+ "\nHASHTAGS=" + hashtags
						);
						jdbi.useHandle(h -> {
							h.execute("insert into tweets values (?, ?, ?, ?, ?)",
								s.getId(),
								s.getUser().getScreenName(),
								s.getText(),
								s.getUser().getLocation(),
								0
							);
							for(String hashtag: hashtags) {
								int i =
									h.execute(
										"update hashtags set tweets = tweets + 1 where hashtag = ?",
										hashtag
									);
								if(i == 0) // No hashtag found, insert
									h.execute("insert into hashtags values (?, ?)", hashtag, 1);
							}
						});
					}	
				})
        .get();
  }

}

package org.openapitools.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

import org.openapitools.model.Popular;
import org.openapitools.model.Tweet;

import java.io.File;
import java.io.IOException;
import io.swagger.annotations.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.validation.Valid;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlite3.SQLitePlugin;

@Controller
@RequestMapping("${openapi.assignment.base-path:}")
public class TweetsApiController implements TweetsApi {
	private Jdbi jdbi;

	@org.springframework.beans.factory.annotation.Autowired
	public TweetsApiController(NativeWebRequest request) throws IOException {
		this.jdbi = Jdbi.create("jdbc:sqlite:../assignment.sqlite3")
			.installPlugin(new SQLitePlugin());
	}

	@Override
	public ResponseEntity<List<Tweet>> getTweet(@ApiParam(value = "Tweet id") @Valid @RequestParam(value = "id", required = false) String id) {
		List<Tweet> tweets = null;

		if(id != null) {
			tweets = this.jdbi.withHandle(h -> {
				return h.createQuery("select * from tweets where id = :id")
					.bind("id", id)
					.mapToBean(Tweet.class)
					.list();
			});
		} else {
			tweets = this.jdbi.withHandle(h -> {
				return h.createQuery("select * from tweets")
					.mapToBean(Tweet.class)
					.list();
			});
		}
		
		return
			tweets.isEmpty()
			? new ResponseEntity<>(HttpStatus.NOT_FOUND)
			: new ResponseEntity<List<Tweet>>(tweets, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Tweet> tweetsIdValidatePut(@ApiParam(value = "Tweet id",required=true) @PathVariable("id") String id) {
		// id is required, no need to check for nullity
		Tweet tweet = this.jdbi.withHandle(h -> {
			if(1 == h.execute("update tweets set validated = 1 where id = ?", id)) {
				return h.createQuery("select * from tweets where id = :id")
					.bind("id", id)
					.mapToBean(Tweet.class)
					.one();
			}
			return null;
		});
		if(tweet == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<Tweet>(tweet, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Popular>> findPopularHashtags(@ApiParam(value = "Number of most used hashtags in our tweet compilation") @Valid @RequestParam(value = "n", required = false, defaultValue="10") Integer n) {
		// n has defaultValue, don't need to check for nullity
		if(n < 0) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		List<Popular> popular = this.jdbi.withHandle(h -> {
			return h.createQuery(
				"select hashtag, tweets from hashtags order by 2 desc limit :n")
				.bind("n", n)
				.mapToBean(Popular.class)
				.list();
		});
		if(popular.isEmpty())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<List<Popular>>(popular, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<Tweet>> findValidatedTweetsByUser(@ApiParam(value = "User name which validated tweets should be fetched",required=true) @PathVariable("user") String user) {
		// user is required, no need to check for nullity
		List<Tweet> tweets = this.jdbi.withHandle(h -> {
			return h.createQuery(
				"select * from tweets where validated = 1 and user = :user")
				.bind("user", user)
				.mapToBean(Tweet.class)
				.list();
		});
		if(tweets.isEmpty())
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<List<Tweet>>(tweets, HttpStatus.OK);
	}
}

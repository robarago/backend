PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
DROP TABLE hashtags;
DROP TABLE tweets;
CREATE TABLE hashtags(hashtag string constraint hashtag primary key, tweets integer);
CREATE TABLE tweets(id string constraint id primary key, user string, text text, location string, validated boolean);
COMMIT;

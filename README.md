# Java Backend

## Decisiones
Para la realización de la prueba he tomado varias decisiones de implementación
basándome únicamente en mis propias preferencias e intepretación del texto. En
condiciones normales habría contactado para aclarar las dudas sobre sobre la
especificación y para estudiar y consensuar las alternativas de implementación.

### Separación del productor *feed* de tweets del microservicio
He decidido implementar el productor de mensajes y el microservicio de
explotación como dos ejecutables separados porque considero que, de este modo,
hay una mejor separación de responsabilidades, dependencias y, sobre todo,
para permitir una distribución más flexible en un entorno contenerizado.

Asímismo, he optado por realizar toda la persistencia en la base de datos.
Me ha parecido la opción más consistente y sencilla.

Para el productor he adaptado el ejemplo `twitter-streaming` obtenido
del repositorio [Spring Uni](https://github.com/craftingjava/craftingjava-examples).
Este ejemplo usa `twitter4j`, como se indica en la especificación, pero he
realizado cambios en los filtros, persistencia y su configuración.

### Tecnología elegida

#### JDBI
He elegido JDBI porque dispone de una interfaz *fluent* muy sencilla y manejable,
un mapeo a objetos muy sencillo y casi cero configuración. Para una solución más
general habría usado un ORM (Hibernate) u Spring Data, pero dado lo limitado del
problema (y del tiempo para la solución) he optado por una tecnología más
*ligera* (aunque desconocida hasta hoy por mi).

#### OpenAPI
He elegido OpenAPI versión 3 en lugar de Swagger 2 porque tengo más experiencia
con la sintaxis de OpenAPI3 y con el uso de esta herramienta.

### SQLite3
También he elegido SQLite3 porque no require una gran infraestructura de
despliegue, es rápida, sencilla y flexible y dispone, asímismo, de un juego de
tipos y de un dialecto SQL estándar. Para un caso real, valoraría usar una base
de datos multiusuario en red como PostgreSQL o MariaDB con JPA, por ejemplo.

## Requisitos
- JDK 8 (probado con OpenJDK 1.8.0_232)
- Maven (probado con 3.3.9)
- openapi-generator (probado 4.2.2)

## Configuración
El *feed* de datos (`twitter-streaming`) **requiere** añadir en el
fichero `twitter-streaming/src/main/resources/twitter4j.properties`)
unas credenciales válidas de dessarrollador Twitter. Por favor,
añadan estas credenciales para ejecutar con éxito la aplicación.

La configuración de *tracks* (términos del *feed*), idiomas y número mínimo de
seguidores de usuarios seguidos se encuentran en el fichero
`twitter-streaming/src/main/resources/application.properties`. Es necesario
recompilar el proyecto si se cambian estos valores, ya que se copian en el
directorio `target`. Aunque las especificaciones no lo mencionan, en las pruebas
realizadas he constadado que **no** es posible usar el interfaz de *streams* de
`twitter4j` con un filtro sin *tracks* o sin *follows*, por lo que he optado
por añadir *tracks* al filtro existente.

## Compilación
Para compilar el proyecto es preciso realizar las siguientes tareas:
1. Generación del API con `openapi-generator`: se requiere tener instalado el
   script `openapi-generator` proporcionado por las
   [openapitools](http://openapitools.org) ya que se usa la versión OpenAPI3.
```
> openapi-generator generate -g spring -o api -i api.yaml --library spring-boot -t template
```
2. Compilación de los dos proyectos usando maven
```
> (cd api && mvn package)
> (cd twitter-streaming && mvn package)
```

## Ejecución
Por razones de *rate limiting*, es probable que si ejecutan (y paran) más de 15
veces en 15 minutos el proyecto `twitter-streaming` el programa sea bloqueado.
Esta es una limitación impuesta a los desarrolladores por Twitter.

Para ejecutar el productor `twitter-streaming`: 
```
> (cd twitter-streaming && mvn spring-boot:run)
```
Para ejecutar el microservicio REST: 
```
> (cd api && mvn spring-boot:run)
```

## Pruebas
El *feeder* se puede probar mientas las credenciales sean válidas. Una vez
ejecutado mostrará en el log los tweets que va recibiendo e insertando en la BD.

El servicio se despliega en http://localhost:3001 y **no** tiene instalado el
`swagger-ui`. Puede probarse usando `cURL`, por ejemplo:

- Consultar los tweets:
  `curl -v -s localhost:3001/tweets` (todos) 
  `curl -v -s localhost:3001/tweets?id=XXXXX` para un tweet concreto
  donde XXXXX es el identificador de un *tweet* existente.
- Marcar un tweet como validado:
  `curl -v -s -XPUT localhost:3001/tweets/XXXXX/validate`
- Consultar los tweets validados por usuario:
  `curl -v -s localhost:3001/tweets/validated/UUUUUUU`
  donde UUUUU es el nombre (*screenName*) de un usuario con algún *tweet* ya
  validado.
- Consultar *n* hashtags más usados:
  `curl -v -s localhost:3001/tweets/popular?n=NN`
